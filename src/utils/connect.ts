import config from 'config';
import mongoose from 'mongoose';
import logger from '../utils/logger';

async function connect() {
  const dbUri = config.get<string>('dbUri');

  try {
    await mongoose.connect(dbUri);
    logger.info(`Connected to DB`);
  } catch (error) {
    logger.error(`Error Connect to DB`);
    process.exit(1);
  }
}

export default connect;
