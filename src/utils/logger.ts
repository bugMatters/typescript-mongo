import winston from 'winston';

const logFormat = winston.format.combine(
  winston.format.timestamp(),
  winston.format.printf(({ timestamp, level, message }) => {
    const ts = timestamp.slice(0, 19).replace('T', ' ');
    return `[${ts}] ${level}: ${message}`;
  })
)

const transports = [
  new winston.transports.Console({
    level: 'info', // Log level for console output
    format: logFormat
  }),

  new winston.transports.File({
    filename: 'error.log', // Path to the error log file
    level: 'error', // Log level for error file
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    format: logFormat
  }),

  new winston.transports.File({
    filename: 'combined.log', // Path to the combined log file
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    format: logFormat
  })
];

const logger = winston.createLogger({
  level: 'info', // Minimum log level to be recorded
  format: logFormat,
  transports
});

export default logger;
