import bodyParser from 'body-parser';
import config from 'config';
import cors from "cors";
import express from 'express';
import helmet from "helmet";
import morgan from "morgan";
import path from "path";

import routes from './routes';
import connect from './utils/connect';
import logger from './utils/logger';

import { morganConfig } from '../config/default.ts';
import deserializeUser from "./middleware/deserializeUser.ts";
import { NotFound } from './middleware/error.ts';

const port = config.get<number>('port');
const logs = config.get<string>("logs")
const node_env = config.get<string>("node_env")

const app = express();

// Request logging. dev: console | production: file
app.use(morgan(logs, node_env === "production" ? morganConfig : {}));

// Mount BodyParser middleware will append body of request to req.body
app.use(bodyParser.json({ limit: '10kb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10kb' }));

// Static assets directory setup
app.use(express.static(path.join(__dirname, '../public')));

// Secure apps by setting various HTTP headers
app.use(helmet());

// Enable CORS - Cross Origin Resource Sharing
app.use(cors());

app.use(deserializeUser)

routes(app);

// Errors Middleware
app.use(NotFound)

app.listen(port, async () => {
  logger.info(`Server is running at http://localhost:${port}`);
  await connect();
});


