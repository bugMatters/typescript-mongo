import { Express } from 'express';

import validateResource from './middleware/validateResource';

import { createUserSessionHandler, deleteSessionHandler, getUserSessionHandler } from './controller/session.controller';
import { createUserHandler } from './controller/user.controller';

import requireUser from './middleware/requireUser';
import { createSessionSchema } from './schema/session.schema';
import { createUserSchema } from './schema/user.schema';

function routes(app: Express) {
  app.post('/api/users', validateResource(createUserSchema), createUserHandler);

  app.post('/api/sessions', validateResource(createSessionSchema), createUserSessionHandler);

  app.get('/api/sessions', requireUser, getUserSessionHandler);

  app.delete('/api/sessions', requireUser, deleteSessionHandler);
}

export default routes;
