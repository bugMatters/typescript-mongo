
import config from "config";
import { NextFunction, Request, Response } from "express";

const env = config.get("node_env")

class APIError extends Error {
   name: string;
   message: string;
   errors: any[];
   status: number;
   isPublic: boolean;
   stack: string;

   constructor({
      message,
      stack,
      errors = [],
      status = 500,
      isPublic = false,
   }: {
      message: string;
      stack: string;
      errors?: any[];
      status?: number;
      isPublic?: boolean;
   }) {
      super(message);
      this.name = this.constructor.name;
      this.message = message;
      this.errors = errors;
      this.status = status;
      this.isPublic = isPublic;
      this.stack = stack;
   }
}

/**
 * Error Handler Sends Stack Trace only during Development Environment
 * @param {Error} err
 * @param {Request} req
 * @param {Response} res
 * @param {next} next
 */
const Handler = (err: any, req: Request, res: Response, next: NextFunction) => {
   const response = {
      code: err.status,
      message: err.message,
      errors: err.errors,
      stack: err.stack,
   };
   if (env === 'production') delete response.stack;
   res.status(response.code).json(response);
   res.end();
};

export const NotFound = (req: Request, res: Response, next: NextFunction) => {
   const err = new APIError({
      message: 'Resource Not Found',
      status: 404,
      stack: '', // You can set an empty string or omit it if you don't have access to a proper stack trace here
   });
   return Handler(err, req, res, next);
};


