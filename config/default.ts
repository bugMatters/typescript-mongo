process.env.ALLOW_CONFIG_MUTATIONS = "true";
import { Request, Response } from 'express';
import fs from "fs";
import path from "path";
interface AppConfig {
  node_env: string;
  port: number;
  dbUri: string;
  saltWorkFactor: number;
  accessTokenTtl: string;
  refreshTokenTtl: string;
  privateKey: string;
  publicKey: string;
  logs?: string; // Add logs property
  Level?: string

}

export const morganConfig = {
  skip(req: Request, res: Response) {
    return res.statusCode <= 400;
  },
  stream: fs.createWriteStream(path.join(__dirname, '../access.log'), {
    flags: 'a',
  }),
};

const config: AppConfig = {
  node_env: "production",
  port: 8000,
  dbUri: 'mongodb://0.0.0.0:27017/express-ts-mongo',
  saltWorkFactor: 10,
  accessTokenTtl: "15m",
  refreshTokenTtl: "1y",

  privateKey: `-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEA0x6jL99x2U+oc4GQONUUlQgGdRrE2krgNDHZFDYqspGlM4cK
aBsUHPRYOw1/JT/3V5nEShz1sh4ODcDfXYy9ds1aWBF+ktXBv7e/wl1yE3dl1qM6
tx5BdmZTqUZs4YrudxN58N39saIsYLOfvOxGqe01CP0wgiIuUTZYTm0cVtC9TdcD
L+MuyAMlFGNbcJbLCEn7EZehmye3jJnA3/lTiVqa5yCW8qpkSNwY9mwtA6FTj+B9
sCYNIMQp2uyuzybrS3wNLdwGvm73EcsT9L7gnI5zOWu1rEsmAkrunTN2BIH4pAyZ
Q2zNR485DVKqPVdEpLv7Oc1yxoxS9ubdJoYwGagkeU5FEr7BQfH3pzcfl0MyyXGo
HpSVy2S+ZjjHxQDVv6qSJfowcYiX8j2hp4pK8tuYkGzusZZxQAxyDa4YjuQhtuVU
vOn/Kiztq6eca63d+S/Kmdj34D3NxUIMCnKwjncetTiuo4Xwq/nGsyar7PBMHmui
L0X7SqKfSnhw0MDDKwfKbN/F7qRlQbTmYX7bGoBssiTZpBf025C5Z4dwuzS38mLR
9mmsKs55YvFN04oDLTLSJu224XOfYCJPHNufNPbxpwWwGze11mv2QP3HmuW+fR5E
RLV9I8qlI3dyBefhXRnokLC8QXCx+ccVOoQ11a8OaZnhisQvdc0gpeo6GG0CAwEA
AQKCAgA/dGv0DaL4gBrPbNfeXR0slhbqx7r3KZ4t0fW/P3Ch72jKDgKwu8LV2RNT
3ICkv48mQ09eP0oR2Tq8I4Z/Zbfkp2BKLCfxFlJ/v0jhsJiDBVYUp0cUlwFyBtuK
e2JqgAmgvLfgD7ka1PWlsxeF/zZWCLfyFbeoxYFKY50zJVRpO6BKbx/EGvKKp3dT
o3Fua8Rmn2XZKcwnCE8A8Y2pRBU5qYP5iDAWi+RMieru1iDJgInPKthcQ3Oar5ss
9AqutLHwJD8J06IrRsr4DCPZYi28oIw9bARU3tlzwvXpRfho7m+/TxIZ0+/3XDL3
sCr++OXm5aFjBD9FMhQVpdo+pAJ8h7Sf+NOXMiCM+alsg2tbjQ7XDdLpVQhZ8Xtp
V/eqLKmHPJsw9ST+5Fr17oPPX2IwTx533PFW5rZj9avK2FAIqyc2Prh4TXzwS4wL
h4Ym6w5d6nlBrjMERxFdoxtr1Ikn1s3ae+j+jPT8yZyx7AtssZoM8AUA+TzfA4PQ
xsK1YXbZwnue1nsi+jnRZxfius+dgtUqqjVIQV63zHyoXWZ6iNaN5VSsJ+zFdqV2
ueuaNJOhgYE8MzpOWda4ChDqO/5ZlLox22eMATPw+MUmJBm0/UcQp/SWrO/3bW+5
6EfzGzPjVsuzHBjjbxaatN/zrs6bhbzy4oyP/BqNRU7wKtVQuQKCAQEA9iCVj2sc
usdGQxhceXEEguFtxHTBnUNWw5j//Ih1ncdvjFI8zEUOEeGd4qmE3TQcs7b3FlIf
3We/P3TthtpFgLvqVOfjXb74PNLr4r7RimIPXuRfRZMo7vV00382Hr73hfzv7G6x
Uw7Nwc/JJNMp1HVKJg3TUGa23EhzZw/V/M+iv1+bguq/+HZaLRj8c3EgTBxSwLCv
tuXSgSZKjIganP6WBm56ag1n9FA6N1LnRj/qos/c+H8h7bgZZvpPjmUWZ8TcBTvP
RgcGSrj7qECNyaa5uVdvfhL5QtEz70y514Y5EkRSGozOrF8BYzIcMI7hwgiPrP5C
42+28MkU3YpBmwKCAQEA25aRxbgXrfHVShgb+DFULpKmWCdW78MsHNOkat1eWrdX
6gZyd6KBT8Bb374EiQsXiS/gJkaj3WjprfujJ3CYan7Cet494SBHdBBdNgqC6V/G
3wYlRNxuZeEDM7ciC8aX2iiXCMey6F4q3NdxeG7VSkcwDOQDZlZ6WV4xM8ywyd/k
YgclPTlBk5FjZhlszqpAqgZuNxBOxcRYAqaEvcoWRQB3ePkT6rXMZiTuMwyEqAST
+3PGo0hqMBacNfo22GjBY2FrkAS+nPlbcLkJA+nBqq/YnekU7iIwLb+41/HJuPt7
OcsoDcbcUptCD0Xonc4RNwRwiEcwXz9ZzcHvRnmSlwKCAQA/pDlH9lhda32dh8UR
mcmi1gt+CvqtYWYj+YHnmJlhDNqpG/KglxKGSSJ8I305fWoWhi7p1J5sr5DiJaft
gK8MFIsJHVTLPsANajSYUuL3t1dmIAMKiHzDHTDyJyilVpfGS5r2NcEgSumlPccz
AG8GgrWDlXuYhJav6wP8GDoqvr3EYkJh3bV515CFrLpKV2wUghZbwNQOpOqDfpU0
NVU8VxAP5a09ku3YXI5RE9y9a3WOV9U5ZBYv07StzfeYSEc82chSih6UauiTlbVs
0hRXmgbYB+XKYbmWL2YQcHR1aD2FnajlOJeQNpizP5fOboOtmm0cPDqreI2DQVtl
1YQVAoIBAQC+vVCMPXecKaeKv9xzv22PMuV5R1q104Pa2mBET1upUv7zknwVvTuz
6bQkaqsH23GrQcHjnZKzPXFWJnG3TY+nWcpW7fs0NVS71YcR7Rl/3CkkCvOOmCbx
Kf4dCuNw9SVsY7ul7clTHJEA2cd/xE5TQfjUCa+GUnHedPoqM5ARGwEHwYswXi6/
HxgHVOh1r9+RkRpljC3QjdehHcHKFeM4NsqQc1NAmoAc7dFW2HXWeHuOfSe7J6jN
cqZoIJtJad0qFTJYnDZypgsupBvXoVIPkMWJjTAYFexhoNrQAP+nRhLoxbZaGTXC
L4i3OZNwl9HOR3X0elnjMmUwXFgakHmrAoIBAQDoRwhkbRFJaMOKjkmuT/WP7Iyj
qSGB0z271hOF9Mx4mvj0WNNx7dKTsMTDPLaf7ow+xOnq7Y7L32tQx7NjioJgdwp+
vhn4k8uG4evY5gUrf9BTkiaPqRp7oupqyr9wGWRgZlF377zDt1Ply8Du++ZFJ2+y
Prwk562fYL/XOGXTgD2NeiZwPUuGn7QCiHU3lm5lLE/Z3ZTds+bGRv+RIWULd/EZ
mqobd44rvoDz7xQPkvxpF+bUZQUhJFondCaeK21pj7WLFh5+S5dz3MH3mY5TDxXw
S5cRSqpzfrLXj4dy8T/BHvo3Itjdk/7QKirudlil53oGutPaU0VJx02kPnrK
-----END RSA PRIVATE KEY-----`,
  publicKey: `-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0x6jL99x2U+oc4GQONUU
lQgGdRrE2krgNDHZFDYqspGlM4cKaBsUHPRYOw1/JT/3V5nEShz1sh4ODcDfXYy9
ds1aWBF+ktXBv7e/wl1yE3dl1qM6tx5BdmZTqUZs4YrudxN58N39saIsYLOfvOxG
qe01CP0wgiIuUTZYTm0cVtC9TdcDL+MuyAMlFGNbcJbLCEn7EZehmye3jJnA3/lT
iVqa5yCW8qpkSNwY9mwtA6FTj+B9sCYNIMQp2uyuzybrS3wNLdwGvm73EcsT9L7g
nI5zOWu1rEsmAkrunTN2BIH4pAyZQ2zNR485DVKqPVdEpLv7Oc1yxoxS9ubdJoYw
GagkeU5FEr7BQfH3pzcfl0MyyXGoHpSVy2S+ZjjHxQDVv6qSJfowcYiX8j2hp4pK
8tuYkGzusZZxQAxyDa4YjuQhtuVUvOn/Kiztq6eca63d+S/Kmdj34D3NxUIMCnKw
jncetTiuo4Xwq/nGsyar7PBMHmuiL0X7SqKfSnhw0MDDKwfKbN/F7qRlQbTmYX7b
GoBssiTZpBf025C5Z4dwuzS38mLR9mmsKs55YvFN04oDLTLSJu224XOfYCJPHNuf
NPbxpwWwGze11mv2QP3HmuW+fR5ERLV9I8qlI3dyBefhXRnokLC8QXCx+ccVOoQ1
1a8OaZnhisQvdc0gpeo6GG0CAwEAAQ==
-----END PUBLIC KEY-----
`
};

config.logs = config.node_env === 'production' ? 'combined' : 'dev';
config.Level = config.node_env === 'production' ? 'combined' : 'dev';

export default config
